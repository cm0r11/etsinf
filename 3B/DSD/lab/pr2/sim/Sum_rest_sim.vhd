----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2019 13:57:26
-- Design Name: 
-- Module Name: Sum_rest_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sum_rest_sim is
generic (srSIZE : integer := 4);
end Sum_rest_sim;


architecture Behavioral of Sum_rest_sim is


component Sum_Rest_nbits is
Generic ( srSIZE : integer:=4 );
    Port ( OPsr : in STD_LOGIC;
           Asr : in STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           Bsr : in STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           Coutsr : out STD_LOGIC;
           Ssr : out STD_LOGIC_VECTOR (srSIZE-1 downto 0));
end component;

signal a, b : std_logic_vector (srSize-1 downto 0) := (others => '0');
signal s : std_logic_vector (srSize-1 downto 0);
signal op, c : std_logic;


begin

SUMR1: Sum_Rest_nbits 
generic map (srSIZE => 4)
port map ( OPsr => op,
           Asr => a,
           Bsr => b,
           Ssr => s,
           Coutsr=> c
          );
op <= '0', '1' after 40 ns;
                           
a <= (0 => '1', others => '0') after 20 ns;

b <= (1 => '1', others => '0') after 40 ns;


end Behavioral;
