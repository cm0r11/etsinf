----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2019 12:15:37
-- Design Name: 
-- Module Name: Full_Adder_simulation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Full_Adder_simulation is

end Full_Adder_simulation;

architecture Behavioral of Full_Adder_simulation is

component Full_Adder is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           Cin : in STD_LOGIC;
           S : out STD_LOGIC;
           faCout : out STD_LOGIC);
end component;


signal a, b, c : STD_LOGIC := '0';

signal S, Cout : STD_LOGIC;

begin


FA1: Full_Adder port map ( A => a,
                           B => b,
                           Cin => c,
                           S => S,
                           faCout => Cout);

a <= '1' after 40 ns;

b <= '1' after 20 ns,
     '0' after 40 ns,
     '1' after 60 ns;
     
c <= '1' after 10 ns,
      '0' after 20 ns,
      '1' after 30 ns,
      '0' after 40 ns,
       '1' after 50 ns,
      '0' after 60 ns,
      '1' after 70 ns;

end Behavioral;
