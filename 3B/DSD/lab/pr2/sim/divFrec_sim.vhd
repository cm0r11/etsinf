----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/13/2019 01:08:54 PM
-- Design Name: 
-- Module Name: divFrec_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity divFrec_sim is
--  Port ( );
end divFrec_sim;

architecture Behavioral of divFrec_sim is

component DivFrec is
    Generic (SIZE : INTEGER := 2); 
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           q : out STD_LOGIC);
end component;

signal clk, rst, q : STD_LOGIC;


begin

df: DivFrec port map ( rst => rst,
                       clk => clk,
                       q => q
);

rst <= '1', '0' after 5ns;

clk <= '0', '1' after 10ns, '0' after 20ns, '1' after 30ns, '0' after 40ns, '1' 
after 50ns, '0' after 60ns, '1' after 70ns, '0' after 80ns, '1' after 90ns, '0' 
after 100ns, '1' after 110ns, '0' after 120ns, '1' after 130ns, '0' after 140ns, '1' after 150ns, '0' after 160ns;



end Behavioral;
