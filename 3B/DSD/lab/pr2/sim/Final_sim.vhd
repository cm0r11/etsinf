----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.03.2019 15:29:43
-- Design Name: 
-- Module Name: Final_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Final_sim is
--  Port ( );
end Final_sim;

architecture Behavioral of Final_sim is

component Final is
    Port ( 
           a_i : in STD_LOGIC_VECTOR (3 downto 0);
           b_i : in STD_LOGIC_VECTOR (3 downto 0);
           op_i : in STD_LOGIC;
           
           clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           
           en_o : out STD_LOGIC_VECTOR(7 downto 0);
           segments : out STD_LOGIC_VECTOR(7 downto 0) );
           
end component;

signal a, b : STD_LOGIC_VECTOR(3 downto 0);
 
signal op, clk, rst : STD_LOGIC;

signal en : STD_LOGIC_VECTOR(7 downto 0);

signal seg : STD_LOGIC_VECTOR(7 downto 0);

constant CLK_PERIOD : time := 10 ns;

begin

F: Final port map (
    a_i => a,
    b_i => b,
    op_i => op,
    clk_i => clk,
    rst_i => rst,
    en_o => en,
    segments => seg
);

--a <= "0001",
--     "1111" after 320 ns;

--b <= "0010",
--     "1111" after 320 ns;
     
--op <= '0',
--      '1' after 320 ns;
      
process
        begin
        clk <= '0';
        wait for CLK_PERIOD/2;
        clk <= '1';
        wait for CLK_PERIOD/2;
end process;

process
    begin

       rst <= '1';
      wait for 2 ns;   
        rst <= '0';
        a <= "0001";
        b <= "0010";
        op <= '0';
        wait for 100 ns;
        a <= "1111";
        b <= "1111";
        op <= '1';
        wait for 100 ns;
        --wait for 5 ns; 
        --ri <= '1';
        --wait for 1 ns; 
        --ri <= '0';
      wait;
        
end process;
      
      

end Behavioral;
