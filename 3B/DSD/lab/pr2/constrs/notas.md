8.25/10

- Semisumador (0.5/0.5)

- Sumador completo 1 bit estructural (0.5/0.5)

- Sumador/Restador completo N bits estructural (1.5/1.5)
    ··- Es correcto, pero la sentencia de generación para el diseño estructural puede organizarse para no tener que especificar, de forma separada, el primer y el último sumador de la serie.
    ··- Debería simularse un rango un poco más amplio de datos de entrada.

- Divisor de frecuencia (0.0/1.0)
    ··- El fichero de simulación no utiliza el mismo componente divisor de frecuencia definido, por lo que no puede simularse. Tiene toda la pinta de que os lo habéis copiado de algún otro diseño. Si no es así, pasad a hablar conmigo y me lo explicáis.

- Conversor hex2display (1.0/1.0)

- Contador en anillo (1.0/1.0)
    ··- No dispone de señal de habilitación.

- Glue logic (0.75/1.0)
    ··- Se genera un latch para la señal "salida_reg", ya que no se especifica el valor de la señal para todas las combinaciones de entrada.

- Sistema integrado para probar en placa (3.0/3.5)
    ··- Se utiliza la salida del divisor de frecuencia como señal de reloj, no como señal de habilitación.

- Bonus: Sumador/Restador completo N bits comportamental (+0.0/+1.0)
    ··- No implementado.
