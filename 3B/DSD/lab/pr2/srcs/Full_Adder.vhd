----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2019 11:28:12
-- Design Name: 
-- Module Name: Full Adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Full_Adder is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           Cin : in STD_LOGIC;
           S : out STD_LOGIC;
           faCout : out STD_LOGIC);
end Full_Adder;

architecture Behavioral of Full_Adder is

component Half_Adder is

Port ( A1 : in STD_LOGIC;
       B1 : in STD_LOGIC;
       S1 : out STD_LOGIC;
       Cout1 : out STD_LOGIC );
       
end component;

signal sh1, ch1, ch2 : std_logic;

begin

HA1: Half_Adder port map ( A1 => A,
                           B1 => B,
                           S1 => sh1,
                           Cout1 => ch1);
                           
HA2: Half_Adder port map ( A1 => sh1,
                           B1 => Cin,
                           S1 => S,
                           Cout1 => ch2);

faCout <= ch1 or ch2;

--Cout <= (A and B) or ( Cin and (A xor B) );
--S <= A xor B xor Cin;

       
       

end Behavioral;
