----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/14/2019 12:55:02 PM
-- Design Name: 
-- Module Name: Final - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Final is
    Port ( 
           a_i : in STD_LOGIC_VECTOR (3 downto 0);
           b_i : in STD_LOGIC_VECTOR (3 downto 0);
           op_i : in STD_LOGIC;
           
           clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           
           en_o : out STD_LOGIC_VECTOR(7 downto 0);
           segments : out STD_LOGIC_VECTOR(7 downto 0) );
           
end Final;

architecture Behavioral of Final is

component RingCounter is
    Generic ( rcSIZE : INTEGER := 8 );
    Port ( rcrst_i : in STD_LOGIC;        -- active high reset
           rcclk_i : in STD_LOGIC;        -- rising edge active clock
                                       -- input bit
           rcoutput : out STD_LOGIC_VECTOR(rcSIZE-1 downto 0));      -- output bit
end component;

component Sum_Rest_nbits is
    Generic ( srSIZE : integer := 4 );
    Port ( OPsr : in STD_LOGIC;
           Asr : in STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           Bsr : in STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           Coutsr : out STD_LOGIC;
           Ssr : out STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           vsr : out STD_LOGIC);
end component;

component DivFrec is
    Generic (dfSIZE : INTEGER := 4); 
    Port ( dfclk_e : in STD_LOGIC;
           dfrst : in STD_LOGIC;
           dfclk_s : out STD_LOGIC);
end component;

component sevensegment is
    Port ( datos : in STD_LOGIC_VECTOR (3 downto 0);
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end component;

signal s_i : STD_LOGIC_VECTOR (3 downto 0);
          signal c_i : STD_LOGIC;
           signal v_i : STD_LOGIC;
           signal rc_i : STD_LOGIC_VECTOR(7 downto 0);
           signal hex_o : STD_LOGIC_VECTOR (3 downto 0);
           signal freq_div : STD_LOGIC;
begin

 df: DivFrec
 generic map (dfSIZE => 100000)
  port map (
    dfclk_e => clk_i,
    dfrst => rst_i,
    dfclk_s => freq_div
);

rc: RingCounter port map (
    rcoutput => rc_i,
    rcrst_i => rst_i,
    rcclk_i => freq_div
);

sr: Sum_Rest_nbits port map (
    OPsr => op_i,
    Asr => a_i,
    Bsr => b_i,
    Coutsr => c_i,
    Ssr => s_i,
    vsr => v_i
);



--TODO: Glue logic

process(rc_i)
begin
    if rc_i = "10000000" then 
--- TODO: port map with glue logic
        hex_o <= a_i;

        
    elsif rc_i = "01000000" then
        hex_o <= b_i;
    elsif rc_i = "00100000" then
        hex_o <= s_i;
        
    elsif rc_i = "00001000" then
        hex_o <= (0 => c_i, others => '0');
        
    elsif rc_i = "00000100" then
        hex_o <= (0 => v_i, others => '0');
    else
        hex_o <= (others => '0');
    
    end if;
    
end process;
sss: sevensegment port map ( 
         datos => hex_o,
                
         salida => segments);
        
         en_o <= not rc_i;
end Behavioral;
