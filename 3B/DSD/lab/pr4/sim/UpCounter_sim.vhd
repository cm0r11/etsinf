----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/26/2019 08:23:16 PM
-- Design Name: 
-- Module Name: UpCounter_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UpCounter_sim is
--  Port ( );
end UpCounter_sim;

architecture Behavioral of UpCounter_sim is

component UpCounter is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           d_i : in STD_LOGIC_VECTOR(2 downto 0);
           q_o : out STD_LOGIC_VECTOR (2 downto 0);
           l_i : in STD_LOGIC);
end component;

signal clk, rst, l : STD_LOGIC;
signal d, q : STD_LOGIC_VECTOR(2 downto 0);

-- Clock period
constant CLK_PERIOD : time := 10 ns;

begin

uc : UpCounter port map (
    clk_i => clk,
    rst_i => rst,
    d_i => d,
    q_o => q,
    l_i => l
);

-- Clock generation
    process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;
        clk <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    process
    begin
        rst <= '1', '0' after 10 ns;
        d <= "000";
        l <= '0';
        wait for CLK_PERIOD;
        
        l <= '0';
        wait for CLK_PERIOD;
        
        l <= '0';
        wait for CLK_PERIOD;
        
        l <= '1';
        d <= "111";
        wait for CLK_PERIOD;
        
        l <= '0';
        wait for CLK_PERIOD;
        
        l <= '0';
        wait for CLK_PERIOD;
        
        l <= '1';
        d <= "100";
        wait for CLK_PERIOD;
        
        l <= '0';
        wait for CLK_PERIOD;
        
        l <= '0';
        wait for CLK_PERIOD;
    end process;
    
     

end Behavioral;
