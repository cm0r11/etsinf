----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2019 09:53:21 AM
-- Design Name: 
-- Module Name: RingCounter_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RingCounter_sim is

end RingCounter_sim;

architecture Behavioral of RingCounter_sim is

component RingCounter is
    Generic ( rcSIZE : INTEGER );
    Port ( rcrst_i : in STD_LOGIC;        -- active high reset
           rcclk_i : in STD_LOGIC;
           rcoutput : out STD_LOGIC_VECTOR(rcSIZE-1 downto 0));      -- output bit
end component;

--signal q : STD_LOGIC_VECTOR (SIZE-1 downto 0);
	--signal aux : STD_LOGIC;
	signal ri, ci : STD_LOGIC;
	signal otp : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
	constant CLK_PERIOD : time := 10 ns;

begin

RC: RingCounter
generic map (rcSIZE => 4)
port map (
          rcrst_i => ri,
          rcclk_i => ci,
          rcoutput => otp
          );
          
process
  begin
  ci <= '0';
  wait for CLK_PERIOD/2;
  ci <= '1';
  wait for CLK_PERIOD/2;
end process;

process
    begin

       ri <= '1';
      wait for 2 ns;   
        ri <= '0';
        --wait for 5 ns; 
        --ri <= '1';
        --wait for 1 ns; 
        --ri <= '0';
      wait;
        
end process;

--ri <= '0';
--ci <= '0', '1' after 20 ns;


end Behavioral;