----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.04.2019 17:08:48
-- Design Name: 
-- Module Name: FSM_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM_sim is
    
end FSM_sim;

architecture Behavioral of FSM_sim is

component FSM is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           is_i : in STD_LOGIC_VECTOR (1 downto 0);
           b12_s : out STD_LOGIC_VECTOR (1 downto 0));
end component;

signal clk, rst : std_logic;
signal insu : std_logic_vector(1 downto 0);
signal b12 : std_logic_vector(1 downto 0);

constant CLK_PERIOD : time := 10 ns;

begin

maq: FSM port map (
    clk_i => clk,
    rst_i => rst,
    is_i => insu,
    b12_s => b12
);

process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;
        clk <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
process
begin
    rst <= '1', '0' after 10 ns;
    
    
     
    insu <= "00";
    
    wait for CLK_PERIOD;
    
    insu <= "10";
        
      wait for CLK_PERIOD;
        
    insu <= "10";
            
       wait for CLK_PERIOD;
            
            
    insu <= "00";
                
       wait for CLK_PERIOD;
                
                
     insu <= "00";
                    
       wait for CLK_PERIOD;
                    
                    
     insu <= "10";
                        
        wait for CLK_PERIOD;
        
     insu <= "10";
                                
        wait for CLK_PERIOD;
     insu <= "00";
                                
          wait for CLK_PERIOD;
                
     insu <= "11";
                                        
         wait for CLK_PERIOD;
     insu <= "11";
                                                
          wait for CLK_PERIOD;
          
          insu <= "10";
                                             
              wait for CLK_PERIOD;
          insu <= "11";
                                                     
               wait for CLK_PERIOD;     
     insu <= "11";
                                             
              wait for CLK_PERIOD;
          insu <= "10";
                                                     
               wait for CLK_PERIOD;     
               
               
      insu <= "11";
                                                  
                   wait for CLK_PERIOD;
       insu <= "10";
                                                          
       wait for CLK_PERIOD;
       
            insu <= "00";
                                          
           wait for CLK_PERIOD;
            
         insu <= "11";
                                               
                wait for CLK_PERIOD;
                 
            
          insu <= "00";
                                                                 
             wait for CLK_PERIOD;
end process;

end Behavioral;
