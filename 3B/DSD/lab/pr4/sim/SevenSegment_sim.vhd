----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2019 10:13:39 AM
-- Design Name: 
-- Module Name: SevenSegment_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SevenSegment_sim is
--  Port ( );
end SevenSegment_sim;

architecture Behavioral of SevenSegment_sim is

component sevensegment is
    Port ( datos : in STD_LOGIC_VECTOR (3 downto 0);
          
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end component;

signal data : std_logic_vector(3 downto 0);
signal output : std_logic_vector(7 downto 0);

begin

uut : SevenSegment port map (
    datos => data,
    salida => output
);


data <= "0000","1000" after 10 ns,"0001" after 20 ns, "0110" after 30 ns;




end Behavioral;
