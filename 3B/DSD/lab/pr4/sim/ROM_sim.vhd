----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/26/2019 08:24:15 PM
-- Design Name: 
-- Module Name: ROM_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ROM_sim is
--  Port ( );
end ROM_sim;

architecture Behavioral of ROM_sim is

    component ROM is
    Port ( a_i : in STD_LOGIC_VECTOR (4 downto 0);
           d_o : out STD_LOGIC_VECTOR (3 downto 0));
    end component;
    
    signal address : std_logic_vector(4 downto 0);
    signal data : std_logic_vector(3 downto 0);

begin

    uut : ROM
    port map (
        a_i => address,
        d_o => data
    );
    
    process
    begin
    
        address <= "00101";
        wait for 10ns;
    
        address <= "10111";
        wait for 10ns;
    
        address <= "11001";
        wait for 10ns;
    
        address <= "00100";
        wait for 10ns;
    
        address <= "10000";
        wait for 10ns;
        
        address <= "11010";
        wait for 10ns;
    end process;

end Behavioral;
