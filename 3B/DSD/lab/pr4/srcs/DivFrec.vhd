----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/28/2019 12:00:52 AM
-- Design Name: 
-- Module Name: DivFrec - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DivFrec is
    Generic (SIZE : INTEGER := 8); 
    Port ( clk_e : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           clk_s : out STD_LOGIC);
end DivFrec;

architecture Behavioral of DivFrec is
signal count : integer := 0;
begin

process(clk_e, rst_i)
begin

    if rst_i = '1' then
        count <= 0;
        clk_s <= '0';
    elsif clk_e'event and clk_e = '1' then
        if count < SIZE-1 then
            count <= count+1;
            clk_s <= '0';
        else
            clk_s <= '1';
            count <= 0;
        end if;
    end if;


end process;

end Behavioral;