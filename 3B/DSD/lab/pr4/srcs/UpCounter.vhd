----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/20/2019 06:34:26 PM
-- Design Name: 
-- Module Name: UpCounter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UpCounter is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           d_i : in STD_LOGIC_VECTOR(2 downto 0);
           q_o : out STD_LOGIC_VECTOR (2 downto 0);
           l_i : in STD_LOGIC);
end UpCounter;

architecture Behavioral of UpCounter is

signal cont : STD_LOGIC_VECTOR(2 downto 0) := "000";
--signal aux1 : unsigned(2 downto 0);

begin

process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            cont <= "101";
        elsif clk_i'event and clk_i = '1' then
            if l_i = '1' then
                cont <= d_i;
            elsif l_i = '0' and cont < "111" then
                --aux1 <= unsigned(cont);
                --aux1 <= aux1+1;
                --cont <= std_logic_vector(aux1);
                cont <= std_logic_vector(unsigned(cont) + 1);
            elsif l_i = '0' and cont = "111" then
                cont <= "000";
            end if;
        end if;
        
end process;

q_o <= cont;


end Behavioral;