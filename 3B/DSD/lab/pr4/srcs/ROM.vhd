----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/21/2019 02:36:39 PM
-- Design Name: 
-- Module Name: ROM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ROM is
    Port ( a_i : in STD_LOGIC_VECTOR (4 downto 0);
           d_o : out STD_LOGIC_VECTOR (3 downto 0));
end ROM;



architecture Behavioral of ROM is


--    type memory is array (0 to 31) of STD_LOGIC_VECTOR(3 downto 0);
    
--    constant memory_content : memory := (
--       0 => "1101",
--       1 => "1111",
--       2 => "1111",
--       4 => "0101",
--       5 => "1101",
--       7 => "1111",
--       16 => "0000",
--       17 => "1001",
--       18 => "1100",
--       20 => "1100",
--       21 => "1001",
--       23 => "1100",
--       24 => "1000",
--       25 => "0010",
--       26 => "1010",
--       28 => "1000",
--       29 => "1000",
--       31 => "1010",
--       others => "1101"
--    );
    


begin

--d_o <= memory_content(to_integer(unsigned(a_i)));
    my_rom: process(a_i)
    begin
        case (a_i) is
            when "00101" => d_o <= "1101";
            when "10101" => d_o <= "1001";
            when "11101" => d_o <= "1000";
            when "00111" => d_o <= "1111";
            when "10111" => d_o <= "1100";
            when "11111" => d_o <= "1010";
            when "00001" => d_o <= "1111";
            when "10001" => d_o <= "1001";
            when "11001" => d_o <= "0010";
            when "00100" => d_o <= "0101";
            when "10100" => d_o <= "1100";
            when "11100" => d_o <= "1000";
            when "00000" => d_o <= "1101";
            when "10000" => d_o <= "0001";
            when "11000" => d_o <= "1000";
            when "00010" => d_o <= "1111";
            when "10010" => d_o <= "1100";
            when "11010" => d_o <= "1010";
            when others => d_o <= "1101";
        end case;

    end process my_rom;

end Behavioral;
