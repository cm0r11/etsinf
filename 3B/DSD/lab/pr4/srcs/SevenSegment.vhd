----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2019 10:12:57 AM
-- Design Name: 
-- Module Name: SevenSegment - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sevensegment is
    Port ( datos : in STD_LOGIC_VECTOR (3 downto 0);
          
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end sevensegment;

architecture Behavioral of sevensegment is

begin
    
--    process ( datos )
--    begin
    
--        if datos = "0000" then
--            salida <= "11000000";
--        elsif datos = "0001" then
--            salida <= "11111001";
--        elsif datos = "0010" then
--            salida <= "10100100";
            
----        elsif datos = "0011" then
----            salida <= "10110000";
----        elsif datos = "0100" then                
----            salida <= "10011001";
----        elsif datos = "0101" then
----            salida <= "10010010";
----        elsif datos = "0110" then
----            salida <= "10000010";
----        elsif datos = "0111" then
----            salida <= "11011000";
----        elsif datos <= "1000" then
----            salida <= "10000000";
----        elsif datos = "1001" then
----            salida <= "10010000";
----        elsif datos = "1010" then
----            salida <= "10001000";
----        elsif datos = "1011" then
----            salida <= "10000011";
----        elsif datos = "1100" then
----            salida <= "11000110";
            
            
--        elsif datos = "1101" then
--            salida <= "11111111";
--            -- oscura
            
--        elsif datos = "1110" then
--            salida <= "10000001";
            
--            --media llena
            
--        elsif datos = "1111" then
--            salida <= "11000001";
            
--            --vasia
            
--        end if;
        
        
--    end process;

    process (datos)
    begin
        if datos = "0000" or datos = "1000" or datos = "0010" or datos = "1010" then
            salida <= "11000000"; -- lleno
        elsif datos = "0001" or datos = "1001" or datos = "0100" or datos = "1100" then
            salida <= "10000001"; --medio lleno
        elsif datos = "0101" or datos = "1101" or datos = "0111" or datos = "1111" then
            salida <= "11000001"; --vacio
        elsif datos = "0110" then --b1
            salida <= "11111001";
        elsif datos = "0011" then --b2
            salida <= "10100100";
        elsif datos = "1011" then
            salida <= "11111111";
        else
            salida <= "10111111";
        end if;
        
    end process;
    
end Behavioral;