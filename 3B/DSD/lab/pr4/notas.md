(9.75/10.0)

Codificación S=Q(t) + Contador ascendente cono carga paralelo + Memoria ROM

- Diagrama y tabla de estados (1.5/1.5)

- Codificación (0.5/0.5)

- Descripción comportamental (2.0/2.0)

- Tabla de excitación y salida (1.5/1.5)

- Descripción estructural (2.0/2.0)

- Simulación (1.5/1.5)

- Integración e implementación (0.75/1.0)
    - Se utiliza la salida del divisor de frecuencia como señal de reloj y no como señal de habilitación.
