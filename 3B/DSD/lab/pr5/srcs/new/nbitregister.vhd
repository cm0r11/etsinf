----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.05.2019 13:04:38
-- Design Name: 
-- Module Name: nbitregister - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity nbitregister is
    Generic (SIZE : INTEGER := 8);
    Port ( 
    entrada : in STD_LOGIC_VECTOR(SIZE-1 downto 0);
       clk : in STD_LOGIC;
       en : in STD_LOGIC;
       PRE : in STD_LOGIC;
       CLR : in STD_LOGIC;
       salida : out STD_LOGIC_VECTOR(SIZE-1 downto 0));
end nbitregister;

architecture Behavioral of nbitregister is

component DFF is
    Port ( D : in STD_LOGIC;
           clk : in STD_LOGIC;
           en : in STD_LOGIC;
           Q : out STD_LOGIC;
           PRE : in STD_LOGIC;
           CLR : in STD_LOGIC);
end component;

begin

nbitregister: for i in 1 to SIZE-1 generate
begin

dffi: DFF port map (
                D => entrada(i),
                clk => clk,
                en => en,
                PRE => PRE,
                CLR => CLR,
                Q => salida(i));
                                        

end generate;

end Behavioral;
