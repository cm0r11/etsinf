----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2019 09:51:33 AM
-- Design Name: 
-- Module Name: RingCounter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RingCounter is
    Generic ( rcSIZE : INTEGER := 8 );
    Port ( rcrst_i : in STD_LOGIC;        -- active high reset
           rcclk_i : in STD_LOGIC;        -- rising edge active clock
                                       -- input bit
           en_i : in STD_LOGIC;
           rcoutput : out STD_LOGIC_VECTOR(rcSIZE-1 downto 0));      -- output bit
end RingCounter;

architecture Behavioral of RingCounter is

    -- auxiliary signals
    signal q : STD_LOGIC_VECTOR (rcSIZE-1 downto 0);
	signal aux : STD_LOGIC;

begin

    -- the state of the register can be updated when the reset
    -- or the clock signals change
    process(rcrst_i, rcclk_i)
    
    begin
         
         if rcrst_i = '1' then                        -- active high reset
            q <= (0 => '1', others => '0');
            
         elsif en_i = '1' and rcclk_i'event and rcclk_i = '1' then    -- clock's rising edge
            q <= q(rcSIZE-2 downto 0) & q(rcSIZE-1);
         end if;
         
    end process;

    rcoutput <= q;-- update output
    
end Behavioral;

