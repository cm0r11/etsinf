----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/13/2019 11:38:48 AM
-- Design Name: 
-- Module Name: DFF - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DFF is
    Port ( D : in STD_LOGIC;
           clk : in STD_LOGIC;
           en : in STD_LOGIC;
           Q : out STD_LOGIC;
           --nQ : out STD_LOGIC;
           PRE : in STD_LOGIC;
           CLR : in STD_LOGIC);
end DFF;

architecture Behavioral of DFF is

begin

    process (clk, CLR, PRE)
    begin 
    if ( CLR = '0' ) then 
    Q <= '0' ; 
    --nQ <= '1';
    elsif (PRE = '0') then
    Q <= '1';
    --nQ <= '0';
    elsif ( rising_edge(clk) and en = '1' ) then
    Q <= D;
    --nQ <= not D;
    
    end if;
    
    end process;
end Behavioral;
