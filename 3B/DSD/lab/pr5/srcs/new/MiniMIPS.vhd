----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/04/2019 12:05:17 PM
-- Design Name: 
-- Module Name: MiniMIPS - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MiniMIPS is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           port0 : out STD_LOGIC_VECTOR (7 downto 0);
           port1 : out STD_LOGIC_VECTOR (7 downto 0);
           port2 : out STD_LOGIC_VECTOR (7 downto 0);
           port3 : out STD_LOGIC_VECTOR (7 downto 0);
           port4 : out STD_LOGIC_VECTOR (7 downto 0);
           port5 : out STD_LOGIC_VECTOR (7 downto 0);
           port6 : out STD_LOGIC_VECTOR (7 downto 0);
           port7 : out STD_LOGIC_VECTOR (7 downto 0)
           );
end MiniMIPS;

architecture Behavioral of MiniMIPS is

component Memory is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           MemWrite_i : in STD_LOGIC;
           Address_i : in STD_LOGIC_VECTOR(7 downto 0);
           WriteData_i : in STD_LOGIC_VECTOR(7 downto 0);
           MemData_o : out STD_LOGIC_VECTOR(7 downto 0);
           port0 : out STD_LOGIC_VECTOR(7 downto 0);
           port1 : out STD_LOGIC_VECTOR(7 downto 0);
           port2 : out STD_LOGIC_VECTOR(7 downto 0);
           port3 : out STD_LOGIC_VECTOR(7 downto 0);
           port4 : out STD_LOGIC_VECTOR(7 downto 0);
           port5 : out STD_LOGIC_VECTOR(7 downto 0);
           port6 : out STD_LOGIC_VECTOR(7 downto 0);
           port7 : out STD_LOGIC_VECTOR(7 downto 0));
end component;

component automatacontrol is
    Port ( instruction : in STD_LOGIC_VECTOR (5 downto 0);
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           PCWriteCond : out STD_LOGIC;
           PCWrite : out STD_LOGIC;
           IorD : out STD_LOGIC;
           MemWrite : out STD_LOGIC;
           MemDataReg : out STD_LOGIC;
           MemToReg : out STD_LOGIC;
           IRWrite : out STD_LOGIC_VECTOR (3 downto 0);
           RegDest : out STD_LOGIC;
           RegWrite : out STD_LOGIC;
           ALUop : out STD_LOGIC_VECTOR (1 downto 0);
           ALUSrcA : out STD_LOGIC;
           ALUSrcB : out STD_LOGIC_VECTOR(1 downto 0);
           PCSource : out STD_LOGIC_VECTOR(1 downto 0));
end component;

component Register8n is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           d_i : in STD_LOGIC_VECTOR (7 downto 0);
           q_o : out STD_LOGIC_VECTOR (7 downto 0));
end component;

component InstructionRegister is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           MemData_i : in STD_LOGIC_VECTOR (7 downto 0);
           IRWrite_i : in STD_LOGIC_VECTOR (3 downto 0);
           Instruction_o : out STD_LOGIC_VECTOR (31 downto 0));
end component;

component ALU is
    Port ( a_i : in STD_LOGIC_VECTOR (7 downto 0);
           b_i : in STD_LOGIC_VECTOR (7 downto 0);
           op_i : in STD_LOGIC_VECTOR (2 downto 0);
           s_o : out STD_LOGIC_VECTOR (7 downto 0);
           zero_o : out STD_LOGIC);
end component;

component ALUcontrol is
    Port ( instruction : in STD_LOGIC_VECTOR (5 downto 0);
           ALUop : in STD_LOGIC_VECTOR (1 downto 0);
           ALUcontrol : out STD_LOGIC_VECTOR (2 downto 0));
end component;

component RegisterFile is
    Port ( ReadReg1_i : in STD_LOGIC_VECTOR(4 downto 0);
           ReadReg2_i : in STD_LOGIC_VECTOR(4 downto 0);
           WriteReg_i : in STD_LOGIC_VECTOR(4 downto 0);
           WriteData_i : in STD_LOGIC_VECTOR(7 downto 0);
           clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           RegWrite_i : in STD_LOGIC;
           ReadData1_o : out STD_LOGIC_VECTOR(7 downto 0);
           ReadData2_o : out STD_LOGIC_VECTOR(7 downto 0));
end component;

component shiftleft2 is
    Port ( entrada : in STD_LOGIC_VECTOR (5 downto 0);
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end component;

---------MIPS---------
--inputs
--outputs
signal sp0, sp1, sp2, sp3, sp4, sp5, sp6, sp7 : std_logic_vector(7 downto 0);

---------MEMORY---------
--inputs

signal wrdata_i: std_logic_vector(7 downto 0);
--outputs
signal memdt_o : std_logic_vector(7 downto 0);


---------PC---------
--inputs
signal pcen : std_logic;
signal pc_i : std_logic_vector(7 downto 0);
--outputs
signal pc_o : std_logic_vector(7 downto 0);

---------MEM_DATA_REGISTER---------
--inputs
signal memdtrg_en : std_logic;
--outputs
signal memdtrg_o : std_logic_vector(7 downto 0);

---------8_IO_REG---------
--inputs
--outpus
signal rgout0, rgout1, rgout2, rgout3, rgout4, rgout5, rgout6, rgout7 : std_logic_vector(7 downto 0);

---------INSTRUCTION_REG---------
--inputs
--outputs
signal ins_o : std_logic_vector(31 downto 0);

---------REGISTER_FILE---------
--inputs
--outputs
signal readt1_o, readt2_o : std_logic_vector(7 downto 0);

---------A---------
--inputs
--outputs
signal aout : std_logic_vector(7 downto 0);

---------B---------
--inputs
--outputs
signal bout : std_logic_vector(7 downto 0);

---------ALUOUT---------
--inputs
--outputs
signal alout : std_logic_vector(7 downto 0);

---------ALU---------
--inputs
--outputs
signal zero : std_logic;
signal result : std_logic_vector(7 downto 0);

---------MUX---------
--mux are numerated from left-right, top-down
--inputs
--outputs

signal mux2_o : std_logic_vector(4 downto 0);
signal mux1_o, mux3_o, mux4_o, mux5_o, mux6_o : std_logic_vector(7 downto 0);

---------SHIFT_LEFT---------
--inputs
--outputs
signal shiftout : std_logic_vector(7 downto 0);

---------CONTROL---------
--inputs
--outputs
signal irwr : std_logic_vector(3 downto 0);
signal pcwrcond, pcwr, iod, memwr, regdest, regwr, alsrca, memtorg : std_logic;
signal alop, alsrcb, pcsrc : std_logic_vector(1 downto 0);

---------ALUCONTROL---------
--inputs
--outputs
signal alctrl : std_logic_vector(2 downto 0);
signal pc_aux_en : std_logic;

begin

mux1_o <= pc_o when (iod = '0') else alout; 
mux2_o <= ins_o(20 downto 16) when (regdest = '0') else ins_o(15 downto 11);
mux3_o <= alout when (memtorg = '0') else memdtrg_o;
mux4_o <= pc_o when (alsrca = '0') else aout;
mux5_o <= bout when (alsrcb = "00") else "00000001" when (alsrcb = "01") else ins_o(7 downto 0); 
mux6_o <= result when (pcsrc = "00") else alout when (pcsrc = "01") else shiftout;

pcen <= pcwr or (zero and pcwrcond);

port0 <= sp0;
port1 <= sp1;
port2 <= sp2;
port3 <= sp3;
port4 <= sp4;
port5 <= sp5;
port6 <= sp6;
port7 <= sp7;

mem : Memory port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => en_i,
    MemWrite_i => memwr,
    Address_i => mux1_o,
    WriteData_i => bout,
    MemData_o => memdt_o,
    port0 => sp0,
    port1 => sp1,
    port2 => sp2,
    port3 => sp3,
    port4 => sp4,
    port5 => sp5,
    port6 => sp6,
    port7 => sp7
);

pc_aux_en <= pcen and en_i;

pc : Register8n port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => pc_aux_en,
    d_i => mux6_o,
    q_o => pc_o
);

mem_data_reg : Register8n port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => memdtrg_en,
    d_i => memdt_o,
    q_o => memdtrg_o
);

rega : Register8n port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => en_i,
    d_i => readt1_o,
    q_o => aout
);

regb : Register8n port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => en_i,
    d_i => readt2_o,
    q_o => bout
);


insreg : InstructionRegister port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => en_i,
    MemData_i => memdt_o,
    IRWrite_i => irwr,
    Instruction_o => ins_o
);

reg_file : RegisterFile port map (
    ReadReg1_i => ins_o(25 downto 21),
    ReadReg2_i => ins_o(20 downto 16),
    WriteReg_i => mux2_o,
    WriteData_i => mux3_o,
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => en_i,
    RegWrite_i => regwr, 
    ReadData1_o => readt1_o,
    ReadData2_o => readt2_o
);

alucom : ALU port map (
    a_i => mux4_o,
    b_i => mux5_o,
    op_i => alctrl,
    s_o => result,
    zero_o => zero
);

alctrlcom : ALUcontrol port map (
    instruction => ins_o(5 downto 0),
    ALUop => alop,
    ALUcontrol => alctrl
);

aloutcom : Register8n port map (
    clk_i => clk_i, 
    rst_i => rst_i,
    en_i => en_i,
    d_i => result,
    q_o => alout
);

shiftl : shiftleft2 port map (
    entrada => ins_o(5 downto 0),
    salida => shiftout
);

fsm : automatacontrol port map (
    instruction => ins_o(31 downto 26), 
    clk => clk_i,
    rst => rst_i,
    en => en_i,
    PCWriteCond => pcwrcond,
    PCWrite => pcwr,
    IorD  => iod,
    MemWrite => memwr,
    MemDataReg => memdtrg_en,
    MemToReg => memtorg,
    IRWrite => irwr,
    RegDest => regdest,
    RegWrite => regwr,
    ALUop => alop,
    ALUSrcA => alsrca,
    ALUSrcB => alsrcb,
    PCSource => pcsrc
);

end Behavioral;
