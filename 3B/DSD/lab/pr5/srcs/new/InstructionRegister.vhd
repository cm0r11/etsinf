----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/03/2019 07:08:59 PM
-- Design Name: 
-- Module Name: InstructionRegister - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity InstructionRegister is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           MemData_i : in STD_LOGIC_VECTOR (7 downto 0);
           IRWrite_i : in STD_LOGIC_VECTOR (3 downto 0);
           Instruction_o : out STD_LOGIC_VECTOR (31 downto 0));
end InstructionRegister;

architecture Behavioral of InstructionRegister is

begin

process(clk_i, rst_i)
begin
    if rst_i = '1' then
        Instruction_o <= (others => '0');
    elsif clk_i'event and clk_i = '1' and en_i = '1' then
        case IRWrite_i is
            when "0001" =>
                Instruction_o(7 downto 0) <= MemData_i;
            when "0010" =>
                Instruction_o(15 downto 8) <= MemData_i;
            when "0100" =>
                Instruction_o(23 downto 16) <= MemData_i;
            when "1000" =>
                Instruction_o(31 downto 24) <= MemData_i;
            when others =>
                --Instruction_o <= (others => '0');
                null;
        end case;
    end if;

end process;


end Behavioral;
