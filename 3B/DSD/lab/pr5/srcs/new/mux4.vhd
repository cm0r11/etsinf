----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.05.2019 11:54:01
-- Design Name: 
-- Module Name: mux4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux4 is
    Port ( a : in STD_LOGIC_VECTOR(7 downto 0);
           b : in STD_LOGIC_VECTOR(7 downto 0);
           c : in STD_LOGIC_VECTOR(7 downto 0);
           d : in STD_LOGIC_VECTOR(7 downto 0);
           sel0 : in STD_LOGIC;
           sel1 : in STD_LOGIC;
           salida : out STD_LOGIC_VECTOR(7 downto 0));
end mux4;




architecture Behavioral of mux4 is

component mux2 is
    Port ( sel : in STD_LOGIC;
           a : in STD_LOGIC_VECTOR(7 downto 0);
           b : in STD_LOGIC_VECTOR(7 downto 0);
           salida : out STD_LOGIC_VECTOR(7 downto 0));
end component;

signal salmuxab, salmuxcd, salida4 : std_logic_vector(7 downto 0);

begin

muxab : mux2 Port map (
                        sel => sel0,
                        a => a,
                        b => b,
                        salida => salmuxab
);

muxcd : mux2 Port map (
                        sel => sel0,
                        a => c,
                        b => d,
                        salida => salmuxcd
);

muxabcd : mux2 Port map (
                        sel => sel1,
                        a => salmuxab,
                        b => salmuxcd,
                        salida => salida
);


--salida <= salida4;

end Behavioral;