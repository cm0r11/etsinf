----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.06.2019 15:57:39
-- Design Name: 
-- Module Name: Display - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Display is
    Port ( datos : in STD_LOGIC_VECTOR (3 downto 0);
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end Display;

architecture Behavioral of Display is

begin

process(datos)
begin 
        if datos(3 downto 0) = "0000" then
            salida <= "11000000";
        elsif datos(3 downto 0) = "0001" then
            salida <= "11111001";
        elsif datos(3 downto 0) = "0010" then
            salida <= "10100100";
        elsif datos(3 downto 0) = "0011" then
            salida <= "10110000";
        elsif datos(3 downto 0) = "0100" then                
            salida <= "10011001";
        elsif datos(3 downto 0) = "0101" then
            salida <= "10010010";
        elsif datos(3 downto 0) = "0110" then
            salida <= "10000010";
        elsif datos(3 downto 0) = "0111" then
            salida <= "11011000";
        elsif datos(3 downto 0) <= "1000" then
            salida <= "10000000";
        elsif datos(3 downto 0) = "1001" then
            salida <= "10010000";
        else
            salida <= "00000000";
        end if;
end process;

end Behavioral;
