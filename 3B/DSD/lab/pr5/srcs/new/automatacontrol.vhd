----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.05.2019 14:10:36
-- Design Name: 
-- Module Name: automatacontrol - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity automatacontrol is
    Port ( instruction : in STD_LOGIC_VECTOR (5 downto 0);
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           PCWriteCond : out STD_LOGIC;
           PCWrite : out STD_LOGIC;
           IorD : out STD_LOGIC;
           MemWrite : out STD_LOGIC;
           MemDataReg : out STD_LOGIC;
           MemToReg : out STD_LOGIC;
           IRWrite : out STD_LOGIC_VECTOR (3 downto 0);
           RegDest : out STD_LOGIC;
           RegWrite : out STD_LOGIC;
           ALUop : out STD_LOGIC_VECTOR (1 downto 0);
           ALUSrcA : out STD_LOGIC;
           ALUSrcB : out STD_LOGIC_VECTOR(1 downto 0);
           PCSource : out STD_LOGIC_VECTOR(1 downto 0));
end automatacontrol;

architecture Behavioral of automatacontrol is

type state is (q0, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18);
signal current_state, next_state : state := q0;

begin

process(rst, clk)
begin
    if rst = '1' then
        current_state <= q0;
        
    elsif clk'event and clk = '1' and en = '1' then
        current_state <= next_state;
    end if;
end process;

transitions : process(instruction,current_state)
begin
--if rst = '0' then
    --current_state <= q0;
--elsif clk'event and clk = '1' and en = '1' then
    case current_state is 
        when q0 =>
            next_state <= q1;
        when q1 =>
            next_state <= q2;
        when q2 =>
            next_state <= q3;
        when q3 =>
            next_state <= q4;
        when q4 =>
            next_state <= q5;
        when q5 =>
            next_state <= q6;
        when q6 =>
            next_state <= q7;
        when q7 =>
            next_state <= q8;
        when q8 =>
            if instruction = "000010" then
                next_state <= q18;
            elsif instruction = "000000" then
                next_state <= q16;
            elsif instruction = "000100" then
                next_state <= q15;
            else 
                next_state <= q9;
            end if;
        when q9 =>
            if instruction = "001000" then
                next_state <= q14;
            elsif instruction = "101000" then
                next_state <= q13;
            elsif instruction = "100000" then
                next_state <= q10;
            else 
                next_state <= q9;
            end if;
        when q10 =>
            next_state <= q11;
        when q11 =>
            next_state <= q12;
        when q12 =>
            next_state <= q0;
        when q13 =>
             next_state <= q0;
        when q14 =>
             next_state <= q0;
        when q15 =>
             next_state <= q0;
        when q16 =>
            next_state <= q17;
        when q17 =>
            next_state <= q0;
        when q18 =>
            next_state <= q0;
        when others =>
            next_state <= q0;
    end case;
--end if;
end process;

--TODO: hay que setear el resto de salidas a 0

outputs: process(current_state)
begin
	PCWriteCond <= '0';
    PCWrite <= '0';
    IorD <= '0';
    MemWrite <= '0';
    MemDataReg <= '0';
    MemToReg <= '0';
    IRWrite <= "0000";
    RegDest <= '0';
    RegWrite <= '0';
    ALUop <= "00";
    ALUSrcA <= '0';
    ALUSrcB <= "00";
    PCSource <= "00";
	case current_state is
        when q1 => 
			ALUSrcB <= "01";
			IRWrite <= "1000";
			PCWrite <= '1';			
        when q3 => 
        	ALUSrcB <= "01";
			IRWrite <= "0100";
			PCWrite <= '1';			
        when q5 => 
            ALUSrcB <= "01";
			IRWrite <= "0010";
			PCWrite <= '1';			   
        when q7 => 
        	ALUSrcB <= "01";
			IRWrite <= "0001";
			PCWrite <= '1';			
        when q8 => 
        	ALUSrcB <= "11";
        when q9 => 
        	ALUSrcA <= '1';
			ALUSrcB <= "10";
		when q10 =>
			IorD <= '1';
        when q11 =>
            MemDataReg <= '1';
		when q12 =>
			RegWrite <= '1';
			MemToReg <= '1';
		when q13 =>
			MemWrite <= '1';
			IorD <= '1';	
		when q14 =>
			RegWrite <= '1';
		when q15 =>
			ALUSrcA <= '1';
			ALUOp <= "01";
			PCWriteCond <= '1';
			PCSource <= "01";		
		when q16 =>
			ALUSrcA <= '1';
			ALUOp <= "10";
		when q17 =>
			RegDest <= '1';
			RegWrite <= '1';
		when q18 =>
			PCWrite <= '1';
			PCSource <= "10";
        when others =>
            PCWriteCond <= '0';
    		PCWrite <= '0';
    		IorD <= '0';
    		MemWrite <= '0';
    		MemDataReg <= '0';
    		MemToReg <= '0';
    		IRWrite <= "0000";
    		RegDest <= '0';
    		RegWrite <= '0';
    		ALUop <= "00";
    		ALUSrcA <= '0';
    		ALUSrcB <= "00";
    		PCSource <= "00";
    end case;
end process;

end Behavioral;
