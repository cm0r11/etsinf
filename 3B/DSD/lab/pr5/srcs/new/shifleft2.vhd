----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.05.2019 12:05:58
-- Design Name: 
-- Module Name: shiftleft2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shiftleft2 is
    Port ( entrada : in STD_LOGIC_VECTOR (5 downto 0);
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end shiftleft2;

architecture Behavioral of shiftleft2 is

begin


-- arithmatic shift left
--salida(7) <= entrada(5);
--salida(6) <= entrada(4);
--salida(5) <= entrada(3);
--salida(4) <= entrada(2);
--salida(3) <= entrada(1);
--salida(2) <= entrada(0);
--salida(1) <= entrada(5);
--salida(0) <= entrada(5);

-- logic shift left
salida(7) <= entrada(5);
salida(6) <= entrada(4);
salida(5) <= entrada(3);
salida(4) <= entrada(2);
salida(3) <= entrada(1);
salida(2) <= entrada(0);
salida(1) <= '0';
salida(0) <= '0';



end Behavioral;
