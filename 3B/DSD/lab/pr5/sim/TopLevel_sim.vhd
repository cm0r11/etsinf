----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.06.2019 18:02:32
-- Design Name: 
-- Module Name: TopLevel_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TopLevel_sim is
--  Port ( );
end TopLevel_sim;

architecture Behavioral of TopLevel_sim is

component TopLevel is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           endis_o : out STD_LOGIC_VECTOR(7 downto 0);
           segments : out STD_LOGIC_VECTOR(7 downto 0));
end component;

signal clk, rst, en : std_logic;
signal endis, seg : std_logic_vector(7 downto 0);
constant CLK_PERIOD : time := 10 ns;


begin

tl : TopLevel port map (
    clk_i => clk,
    rst_i => rst,
    en_i => en,
    endis_o => endis,
    segments => seg
);

process
begin
    clk <= '0';
    wait for CLK_PERIOD/2;
    clk <= '1';
    wait for CLK_PERIOD/2;
end process;

rst <= '1', '0' after 10 ns;
en <= '0', '1' after 10 ns;

end Behavioral;
