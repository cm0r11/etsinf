----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/03/2019 08:55:42 PM
-- Design Name: 
-- Module Name: automatacontrol_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity automatacontrol_sim is
--  Port ( );
end automatacontrol_sim;

architecture Behavioral of automatacontrol_sim is

component automatacontrol is
    Port ( instruction : in STD_LOGIC_VECTOR (5 downto 0);
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           PCWriteCond : out STD_LOGIC;
           PCWrite : out STD_LOGIC;
           IorD : out STD_LOGIC;
           MemWrite : out STD_LOGIC;
           MemDataReg : out STD_LOGIC;
           MemToReg : out STD_LOGIC;
           IRWrite : out STD_LOGIC_VECTOR (3 downto 0);
           RegDest : out STD_LOGIC;
           RegWrite : out STD_LOGIC;
           ALUop : out STD_LOGIC_VECTOR (1 downto 0);
           ALUSrcA : out STD_LOGIC;
           ALUSrcB : out STD_LOGIC_VECTOR(1 downto 0);
           PCSource : out STD_LOGIC_VECTOR(1 downto 0));
end component;

signal clk_s, rst_s, en_s : std_logic;--inputs
signal ins_s : std_logic_vector(5 downto 0);
--outputs
signal irw_s : std_logic_vector(3 downto 0);
signal alop_s, alusrcb_s, pcsource_s : std_logic_vector(1 downto 0);
signal pcwcond_s, pcwrite_s, iord_s, memwr_s, memdatar_s, memtoreg_s, regdest_s, regwr_s, alusrca_s : std_logic;

constant CLK_PERIOD : time := 10 ns;

begin

fsm: automatacontrol port map (
    instruction => ins_s,
    clk => clk_s,
    rst => rst_s,
    en => en_s,
    PCWriteCond => pcwcond_s,
    PCWrite => pcwrite_s,
    IorD => iord_s,
    MemWrite => memwr_s,
    MemDataReg => memdatar_s,
    MemToReg => memtoreg_s,
    IRWrite => irw_s,
    RegDest => regdest_s,
    RegWrite => regwr_s,
    ALUop => alop_s,
    ALUSrcA => alusrca_s,
    ALUSrcB => alusrcb_s,
    PCSource => pcsource_s
);

process
begin
    clk_s <= '0';
    wait for CLK_PERIOD/2;
    clk_s <= '1';
    wait for CLK_PERIOD/2;
end process;

process
begin
    rst_s <= '1', '0' after 10 ns;
    en_s <= '0', '1' after 10 ns;
    
    ins_s <= "000000";
    wait for CLK_PERIOD*4;
    
    ins_s <= "001000";
    wait for CLK_PERIOD*4;
    
    ins_s <= "000100";
    wait for CLK_PERIOD*4;
        
        
    ins_s <= "000010";
    wait for CLK_PERIOD*4;
    
    ins_s <= "100000";
    wait for CLK_PERIOD*4;
    
    ins_s <= "101000";
    wait for CLK_PERIOD*4;
end process;

end Behavioral;
