----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/04/2019 12:31:58 PM
-- Design Name: 
-- Module Name: MiniMIPS_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MiniMIPS_sim is
--  Port ( );
end MiniMIPS_sim;

architecture Behavioral of MiniMIPS_sim is

component MiniMIPS is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           port0 : out STD_LOGIC_VECTOR (7 downto 0);
           port1 : out STD_LOGIC_VECTOR (7 downto 0);
           port2 : out STD_LOGIC_VECTOR (7 downto 0);
           port3 : out STD_LOGIC_VECTOR (7 downto 0);
           port4 : out STD_LOGIC_VECTOR (7 downto 0);
           port5 : out STD_LOGIC_VECTOR (7 downto 0);
           port6 : out STD_LOGIC_VECTOR (7 downto 0);
           port7 : out STD_LOGIC_VECTOR (7 downto 0)
           );
end component;

signal clk, rst, en : std_logic;
signal p0, p1, p2, p3, p4, p5, p6, p7 : std_logic_vector(7 downto 0);

constant CLK_PERIOD : time := 10 ns;

begin

minmips : MiniMIPS port map (
    clk_i => clk,
    rst_i => rst,
    en_i => en,
    port0 => p0,
    port1 => p1,
    port2 => p2,
    port3 => p3,
    port4 => p4,
    port5 => p5,
    port6 => p6,
    port7 => p7
);

process
begin
    clk <= '0';
    wait for CLK_PERIOD/2;
    clk <= '1';
    wait for CLK_PERIOD/2;
end process;

rst <= '1', '0' after 10 ns;
en <= '0', '1' after 10 ns;

--process
--begin
--    rst <= '1';
--    en <= '0';
--    wait for 10 ns;
--    rst <= '0';
--    en <= '1';
--    wait;
    
--end process;

end Behavioral;
