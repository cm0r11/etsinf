----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.03.2019 11:53:42
-- Design Name: 
-- Module Name: VGAController - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGAController is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           blu_i : in STD_LOGIC_VECTOR (3 downto 0);
           grn_i : in STD_LOGIC_VECTOR (3 downto 0);
           red_i : in STD_LOGIC_VECTOR (3 downto 0);
           hs_o : out STD_LOGIC;
           vs_o : out STD_LOGIC;
           blu_o : out STD_LOGIC_VECTOR (3 downto 0);
           grn_o : out STD_LOGIC_VECTOR (3 downto 0);
           red_o : out STD_LOGIC_VECTOR (3 downto 0));
end VGAController;

architecture Behavioral of VGAController is

signal clk, pver : STD_LOGIC;
signal cpix : integer := 0;
signal clin : integer := 0;

component DivFrec is
    Generic (dfSIZE : INTEGER := 4); 
    Port ( dfclk_e : in STD_LOGIC;
           dfrst : in STD_LOGIC;
           dfclk_s : out STD_LOGIC);
end component;

begin

 df: DivFrec
 generic map (dfSIZE => 4)
  port map (
    dfclk_e => clk_i,
    dfrst => rst_i,
   dfclk_s => clk
);



process( clk, rst_i)
begin
if rst_i = '1' then
  cpix <= 0;
  
elsif clk'event and clk = '1' then

  if cpix < 799 then
	cpix <= cpix + 1;
	pver <= '0';
  else
	cpix <= 0;
	pver <= '1';
  end if;
  
end if;
end process;

process(rst_i, pver)
begin
if rst_i = '1' then
  clin <= 0;
  
elsif pver'event and pver = '1' then

    if clin < 520 then
     clin <= clin + 1;
    else 
     clin <= 0;
    end if;
  
end if;
end process;


hs_o <= '0' when cpix >= 0 and cpix < 96 else '1';
vs_o <= '0' when clin >= 0 and clin < 2 else '1';

blu_o <= blu_i when (cpix >= 144 and cpix <= 784) and (clin >= 31 and clin <= 511) else "0000";
grn_o <= grn_i when (cpix >= 144 and cpix <= 784) and (clin >= 31 and clin <= 511) else "0000";
red_o <= red_i when (cpix >= 144 and cpix <= 784) and (clin >= 31 and clin <= 511) else "0000";


end Behavioral;


