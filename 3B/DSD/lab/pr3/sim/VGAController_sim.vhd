----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.04.2019 11:21:52
-- Design Name: 
-- Module Name: VGAController_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGAController_sim is
--  Port ( );
end VGAController_sim;

architecture Behavioral of VGAController_sim is

component VGAController is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           blu_i : in STD_LOGIC_VECTOR (3 downto 0);
           grn_i : in STD_LOGIC_VECTOR (3 downto 0);
           red_i : in STD_LOGIC_VECTOR (3 downto 0);
           hs_o : out STD_LOGIC;
           vs_o : out STD_LOGIC;
           blu_o : out STD_LOGIC_VECTOR (3 downto 0);
           grn_o : out STD_LOGIC_VECTOR (3 downto 0);
           red_o : out STD_LOGIC_VECTOR (3 downto 0));
end component;

signal clk_i, rst_i : STD_LOGIC := '1' ;
signal blu_i, grn_i, red_i : STD_LOGIC_VECTOR (3 downto 0) := "0000";
signal hs_o, vs_o : STD_LOGIC;
signal blu_o, grn_o, red_o : STD_LOGIC_VECTOR (3 downto 0);

begin

vga: VGAController port map (
    clk_i => clk_i,
    rst_i => rst_i,
    blu_i => blu_i,
    grn_i => grn_i,
    red_i => red_i,
    hs_o => hs_o,
    vs_o => vs_o,
    blu_o => blu_o,
    grn_o => grn_o,
    red_o => red_o
);

rst_i <= '0' after 70 ns;
clk_i <= not clk_i after 5 ns;

red_i <= "1010" after 70 ns;
grn_i <= "0101" after 70 ns;
blu_i <= "1100" after 70 ns;



end Behavioral;
