----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.03.2019 11:34:16
-- Design Name: 
-- Module Name: DivFrec_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DivFrec_sim is
--  Port ( );
end DivFrec_sim;

architecture Behavioral of DivFrec_sim is

component DivFrec is
    Generic (dfSIZE : INTEGER := 4); 
    Port ( dfclk_e : in STD_LOGIC;
           dfrst : in STD_LOGIC;
           dfclk_s : out STD_LOGIC);
end component;


signal clk, rst, q : STD_LOGIC;


begin

df: DivFrec port map ( dfclk_e => clk,
           dfrst => rst,
           dfclk_s => q
);

rst <= '1', '0' after 5ns;

clk <= '0', '1' after 10ns, '0' after 20ns, '1' after 30ns, '0' after 40ns, '1' 
after 50ns, '0' after 60ns, '1' after 70ns, '0' after 80ns, '1' after 90ns, '0' 
after 100ns, '1' after 110ns, '0' after 120ns, '1' after 130ns, '0' after 140ns, '1' after 150ns, '0' after 160ns;

end Behavioral;
