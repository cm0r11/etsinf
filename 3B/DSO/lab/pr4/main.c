#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>

extern int errno;

int main(int argc, char *argv[]) {
  pid_t pid;
  long res;
  
  if (argc <= 1) {
    printf("Error: necesito un pid.\n"); 
  }
  else {
    pid = atoi(argv[1]);
    res = syscall(337,pid);
    if (res == 0) {
      printf("Soy el proceso init.\n");
    }
    else if (res < 0) {
      printf("Error: no existe el proceso.\n");
    }
    else {
      printf("Soy de la generación: %ld\n", res);
    }
  }
  return 0;
}