#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
MODULE_LICENSE("Dual BSD/GPL");

int n = 1;
module_param(n, int, S_IRUGO);

static int __init entrando(void) {
  acumular(n);
  return 0;
}

static void __exit saliendo(void) {
  printk(KERN_NOTICE "Instante de extracción: %d\n", llevamos());
}

module_init(entrando);
module_exit(saliendo);
