#sorted list of words in website
from urllib2 import urlopen
import sys, re, unicodedata

if len(sys.argv) < 2:
    dir = "https://es.wikipedia.org/"
else:
    dir = sys.argv[1]

url = urlopen(dir)
data = url.read().decode('utf-8')
words = ''.join((c for c in unicodedata.normalize('NFD',
    data) if unicodedata.category(c) != 'Mn')) #remove accents
words = words.encode('utf-8')

new_list = []
list = re.split(r'[\s:=<>/\()?!"&]', words)
for w in list:
    if w.isalpha() and w not in new_list:
        new_list.append(w)
new_list.sort()

print new_list
