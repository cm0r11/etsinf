import sys, getopt, random, math

def prng(min, max):
    return math.floor(random.random() * ((max - min) + 1) + min)

if len(sys.argv) < 7:
    for i in range(25):
        print(prng(0, 100))
else:
    try:
        opts, args = getopt.getopt(sys.argv[1:], "n:m:M:")
        dic_opts = dict(opts)
        times = int(dic_opts['-n'])
        min = int(dic_opts['-m'])
        max = int(dic_opts['-M'])
        for i in range(times):
            print(prng(min, max))
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
