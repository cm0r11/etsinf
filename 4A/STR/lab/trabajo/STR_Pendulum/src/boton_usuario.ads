package Boton_Usuario is

   procedure Initialize (Use_Rising_Edge : Boolean := True) with
     Pre  => not Initialized,
     Post => Initialized;

   function Has_Been_Pressed return Boolean with
     Pre => Initialized;
   --  Returns whether the user button has been pressed since the last time
   --  this subprogram was called.

   function Initialized return Boolean;

end Boton_Usuario;
