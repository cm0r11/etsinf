#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main( int argc, char *argv[] ) {

  long int n;

  if (argc < 2) {
    fprintf(stderr, "Uso: %s n\n", argv[0]);
    return -1;
  }
  
  sscanf(argv[1], "%ld", &n);

  
  double **a = (double **)malloc(n*sizeof(double*));
  double **b = (double **)malloc(n*sizeof(double*));
  double **c = (double **)malloc(n*sizeof(double*));
  double *A = (double *)malloc(n*n*sizeof(double));
  double *B = (double *)malloc(n*n*sizeof(double));
  double *C = (double *)malloc(n*n*sizeof(double));
  long int i, j, k;
  for (i = 0; i < n; i++) {
    a[i] = &(A[i*n]);
  }
  for (i = 0; i < n; i++) {
    b[i] = &(B[i*n]);
  }
  for (i = 0; i < n; i++) {
    c[i] = &(C[i*n]);
  }
  double time;
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      a[i][j] = ((double) rand()) / RAND_MAX; 
    }
  }

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      b[i][j] = ((double) rand()) / RAND_MAX;
    }
  }


  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      c[i][j] = 0.0;
    }
  }

  time = omp_get_wtime();
  int nhilos;
  #pragma omp parallel for private(i, j, k)
   for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
            
      for (k = 0; k < n; k++) {
	c[i][j] = c[i][j] + (a[i][k] * b[k][j]);
      }
    }
    nhilos = omp_get_num_threads();
  }

  time = omp_get_wtime() - time;

  printf("Tiempo paralelo (num_hilos=%d, tam_prob=%d): %f\n", nhilos, n, time);

  free(a); 
  free(b); 
  free(c); 
  free(A); 
  free(B); 
  free(C); 
  
}
  
