#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main( int argc, char *argv[] ) {
	int i;
	#pragma omp parallel for
	for (i = 0; i < omp_get_num_threads(); i++) {
		printf("ID de hilo: %d\n", omp_get_thread_num());
		#pragma omp single nowait
		printf("Numero de hilos: %d\n", omp_get_num_threads());
	}
	
	printf("Version de OpenMP: %d\n", _OPENMP);
}
