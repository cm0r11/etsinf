int isPrime(int n) {
  int i, prm = 1;
  #pragma omp parallel private(i)
  {
    int id = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    for (i = id+2; i <= n/2 && prm; i+=nthreads) {
      if (n%i==0) prm=0;
    }
  }
  return prm;
}
