#include <stdio.h>
#include <mpi.h>
#include <omp.h>
#include <time.h>
#include <math.h>


#define SOLUCION_ENCONTRADA 100
#define BUSCAR_PRIMO 200
#define FIN 300
#define ESCLAVO_PREPARADO 400

int isPrime(int n) {
  int i, prm = 1;
  #pragma omp parallel private(i)
  {
    int id = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    for (i = id+2; i <= n/2 && prm; i+=nthreads) {
      if (n%i==0) prm=0;
    }
  }
  return prm;
}
  
int main (int argc, char *argv[])
{
    int n, myid, numprocs, i, esclavos, encontrado, continuar, max, nprimos;
    
    double t;
    
     
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Status stat;
    if (argc == 2) max = atoi(argv[1]);
    else max = 5;
    srand(time(0));
    n, nprimos = 0;
	t = MPI_Wtime()*1000;
    if (myid == 0) { //maestro
        i = 0;
        encontrado = 0;
        esclavos = numprocs - 1;
        while (esclavos > 0) {
           //recibir msg de cualquier esclavo
	      MPI_Recv(&n, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	      if (stat.MPI_TAG == SOLUCION_ENCONTRADA) {
	      //imprimir num primo enviado por esclavo
	         if (nprimos < max) {
	            printf("Numero primo %d de esclavo %d\n", n, stat.MPI_SOURCE);
	            nprimos++;
	            n = rand();
		        MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, BUSCAR_PRIMO, MPI_COMM_WORLD);
	         }
	         else {
	            MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, FIN, MPI_COMM_WORLD);
		        esclavos--;
	         }
	      else {
	        n = rand();
		    //enviar num (ETIQ=BUSCAR_PRIMO) al esclavo que envio msg anterior
		    MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, BUSCAR_PRIMO, MPI_COMM_WORLD);
		  }
        }

    } else {  //esclavos
	   MPI_Send(&n, 1, MPI_INT, 0, ESCLAVO_PREPARADO, MPI_COMM_WORLD);
	   continuar = 1;
	   while (continuar) {
	   //recibir msg con numero = n
	   MPI_Recv(&n, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	   if (stat.MPI_TAG == FIN) {
	      continuar = 0;
	   }
	   else {
	      //averiguar si n es primo
          int primo;
          primo = isPrime(n);
  	      if (primo) {
	         //enviar n a maestro (ETIQ=SOLUCION_ENCONTRADA)
		     MPI_Send(&n, 1, MPI_INT, 0, SOLUCION_ENCONTRADA, MPI_COMM_WORLD);
		  }
	      else {
	         //enviar msg al maestro (ETIQ=ESCLAVO_PREPARADO)
		     MPI_Send(&n, 1, MPI_INT, 0, ESCLAVO_PREPARADO, MPI_COMM_WORLD);
	      }
	   }
	  }
    }
    t = MPI_Wtime()*1000 - t;
    if (myid == 0) {
       printf("Tiempo de ejecucion %lf, nprocs %d\n", t, numprocs);
    }

    MPI_Finalize();
}


