#include <stdio.h>
#include <mpi.h>



int main (int argc, char *argv[])
{
    
    int n, nreps, myid, numprocs, j;
    double tc, ts, tw, twn;
 
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

    MPI_Status stat;
    if (argc == 3) {
       n = atoi(argv[1]);
       nreps = atoi(argv[2]);
    }
    else { 
       n = 100000;
       nreps = 100;
    }
    char msg[n];
    ts = MPI_Wtime()*1000;
    tw = 0.0;
   
    for (j = 0; j < nreps; j++) {
          if (myid == 0) {
          twn = MPI_Wtime()*1000;
	  MPI_Send(msg, n, MPI_BYTE, 1, 100, MPI_COMM_WORLD);
	  MPI_Recv(msg, n, MPI_BYTE, 1, 100, MPI_COMM_WORLD, &stat);
	  twn = (MPI_Wtime()*1000) - twn;
	  tw += twn;
       }
       if (myid == 1) {
          MPI_Recv(msg, n, MPI_BYTE, 0, 100, MPI_COMM_WORLD, &stat);
	  MPI_Send(msg, n, MPI_BYTE, 0, 100, MPI_COMM_WORLD);
       }
     }
    
    ts = (MPI_Wtime()*1000) - ts;

    if (myid == 0) {
           
       tc = ts + (n*(tw / nreps));

       printf("Tiempo comunicación (ts): %lf\n", ts);
       printf("Tiempo comunicación (tw) (media de %d repeticiones): %lf\n", nreps, tw/nreps);
       printf("Tiempo comunicación (tc): %lf\n", tc);
    }
    MPI_Finalize();
    return 0;
}
