int nneighbors, max_neighbors = 3;
int neighbors[max_neighbors];

// Cada proceso obtiene su numero de vecinos en nneighbors
MPI_Graph_neighbors_count( MPI_COMM_GRAPH, rank, &nneighbors );
// Cada proceso obtiene en el vector neighbors el identificador de sus vecinos
MPI_Graph_neighbors( MPI_COMM_GRAPH, rank, max_neighbors, neighbors);
