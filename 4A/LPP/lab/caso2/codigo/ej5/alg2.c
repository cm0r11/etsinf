void esclavo( int n, double *x, int t, double *I, double *R, int lda, double *b, double nrm ) {
  int k, inc = 1;
  if (n == 0) {
    if (nrm < minimo) {
      minimo = nrm;
      dcopy_( &lda, x, &inc, sol, &inc );
    }
   } else {
     for( k=0; k<t; k++ ) {
       int m = n-1;
       x(m) = I(k);
       double r = R(m,m)*x(m) - b(m);
       double norma = nrm + r*r;
       if( norma < minimo ) {
       double v[m];
       double y[lda];
       dcopy_( &m, b, &inc, v, &inc );
       dcopy_( &lda, x, &inc, y, &inc );
       double alpha = -x(m);
       daxpy_( &m, &alpha, &R(0,m), &inc, v, &inc );
       esclavo( m, y, t, I, R, lda, v, norma );
     }
   }
  }  
}
