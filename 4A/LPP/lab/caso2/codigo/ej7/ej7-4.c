int k;
double C = 0.0, a_loc, b_loc;

for( k = 0; k < 2; k++ ) {
  a_loc = a;
  b_loc = b;

  // Broadcast de la columna k
  if (coords[1] == k) {
    // k = 0 --> P00 difunde su b (0.2) al proceso P10
    // k = 1 --> P11 difunde su b (6.8) al proceso P01
    MPI_Bcast(&b_loc, 1, MPI_DOUBLE, k, commcol);
  } else {
    // k = 0 --> P01 difunde su b (9.4) al proceso P11
    // k = 1 --> P10 difunde su b (3.7) al proceso P00
    MPI_Bcast(&b_loc, 1, MPI_DOUBLE, k, commcol);
  }

  // Broadcast de la fila
  if (coords[0] == k) {
    // k = 0 --> P00 difunde su a (6.0) al proceso P01
    // k = 1 --> P11 difunde su a (8.0) al proceso P10
    MPI_Bcast(&a_loc, 1, MPI_DOUBLE, k, commrow);
  } else {
    // k = 0 --> P10 difunde su a (0.2) al proceso P11
    // k = 1 --> P01 difunde su a (5.8) al proceso P00
    MPI_Bcast(&a_loc, 1, MPI_DOUBLE, k, commrow);
  }

  // Actualizacion del elemento C
  C = C + a_loc * b_loc;
  }
